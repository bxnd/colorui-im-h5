var app = {
	data(){
		return {
		
		}
	},
	methods:{
		random_chat_get_lists(params){
			return this.$store.dispatch('random_chat/get_lists',params).then(res=>{
				return res
			})
		},
		random_chat_join(params){
			return this.$store.dispatch('random_chat/join',params).then(res=>{
				return res
			})
		},
		random_chat_update_list(data){
			// data = {
			// 	type:'random_chat_joining',
			// 	random_chat_id:'',
			// 	count:'',
			// }
			var random_chat_lists = JSON.parse(JSON.stringify(this.random_chat.lists))
			
			random_chat_lists = random_chat_lists.map((list)=>{
				if(list.id==data.content.id){
					list= data.content
				}
				return list
			})
			
			this.$store.dispatch('random_chat/lists',random_chat_lists)
		},	
		
		random_chat_apply(params){
			return this.$store.dispatch('random_chat/apply',params)
		},	
		random_chat_vote(params){
			return this.$store.dispatch('random_chat/vote',params)
		},
		
		random_chat_update_vote_group_message(data){
			console.log(this.$store.state.im.messages,'dsadsadsad')
			var messages = JSON.parse(JSON.stringify(this.$store.state.im.messages))
			
			messages = messages.map((message)=>{
				if(message.group_id==data.content.to_id){
					message.messages = message.messages.map((item)=>{
						if(item.unique_slug==data.content.unique_slug){
							item = data.content;
						}
						return item;
					})
				}
				return message
			})
			this.$store.dispatch('im/messages',messages)
		}
		
	},
	created(){
		
	},
	computed:{
		random_chat(){
			return this.$store.state.random_chat
		}
	}
}

export default app