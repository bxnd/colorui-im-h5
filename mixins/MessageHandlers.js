    var messageHandlers = {
      gamestart: handleGameStart_,
      update: handleUpdatePlayer_,
      upgame: handleGameMsg_,
      remove: handleRemovePlayer_,
      start: handleStartPlayer_,
      system: handleSystemMsg_,
      log: handleLogMsg_,
    };
	