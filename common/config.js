const config = {
	// 'api_domain':'https://colorui-im-admin.jc91715.top',
	'api_domain':'https://colorui-im-admin.test.jc91715.top',
	// 'api_domain':'http://49.08wx.cn',
	// 'api_domain':'http://colorui-im-admin-docker.test',
	// 'api_domain':'http://127.0.0.1:8880',
	chat_img_domain:'https://colorui-im-admin.test.jc91715.top',
	// 'wbsocket_url':'wss://colorui-im-h5.jc91715.top:8284',
	'wbsocket_url':'wss://colorui-im-workerman-center.test.jc91715.top',
	// 'wbsocket_url':'wss://colorui-im-workerman-center-v1.test.jc91715.top',
	// 'wbsocket_url':'ws://49.08wx.cn:8284',
	// 'wbsocket_url':'ws://47.96.15.116:8284',
	// 'wbsocket_url':'ws://127.0.0.1:8284',
	fake:false
}

export default config