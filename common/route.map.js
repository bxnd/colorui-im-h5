module.exports = {
	"api.login": "api\/sanctum\/token",
	"api.user.users": "api\/user\/users",
	"api.user.show": "api\/user",
	"api.im.bindUid": "api\/im\/bindUid",
	"api.im.send": "api\/im\/send",
	"api.im.joinGroup": "api\/im\/joinGroup",
	"api.im.messages": "api\/im\/messages",
	"api.im.getLatestMessages": "api\/im\/getLatestMessages",
	"api.randomchat.byGroupId": "api\/randomchat\/byGroupId",
	"api.randomchat.join": "api\/randomchat\/join",
	"api.randomchat.apply": "api\/randomchat\/apply",
	"api.randomchat.vote": "api\/randomchat\/vote",
	"api.messageaction.vote": "api\/messageaction\/vote",
	"api.post.index": "api\/post\/index",
	"api.post.publish": "api\/post\/publish",
	"api.post.joinGroup": "api\/post\/joinGroup",
	"api.im.bindGroupUid": "api\/im\/bindGroupUid",
	"api.im.groupSend": "api\/im\/groupSend",
	"api.upload.image": "api\/upload\/image",
	"api.upload.file": "api\/upload\/file",
	"api.upload.video": "api\/upload\/video",
	"api.upload.audio": "api\/upload\/audio",
	"api.upload.fileAudio": "api\/upload\/fileAudio"
};
