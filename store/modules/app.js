
import config from '@/common/config.js'

const state = {
	name:'jcc',
	api_domain: config.api_domain,
	users:[],
	option:{},
	
	btn_list:['引用']
}

const mutations = {

	users: (state, users) => {
		state.users = users
	},
	option: (state, option) => {
		state.option = option
	},
	btn_list: (state, btn_list) => {
		state.btn_list = btn_list
	},



}

const actions = {
	users({commit,state}, { ...params}){
		return this._vm.$request.get('api.user.users',{...params}).then(res=>{
			if(res.data.code==0){
				commit('users',res.data.data.lists)
			}
			return res;
		})
	},
	option({commit,state}, option){
		commit('option',option)
	},
	btn_list({commit,state}, btn_list){
		commit('btn_list',btn_list)
	},
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
