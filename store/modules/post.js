
import config from '@/common/config.js'

const state = {
	posts:[],
	posts_latest_messages:[],//记录了朋友圈的消息数量(2021-04-23)，和 im 中的latest_messages 结构一样，im中 latest_messages 只能获取到自己相关的群聊的 最新消息，可能没有包涵 posts_latest_messages 中的内容
}

const mutations = {

	posts: (state, posts) => {
		state.posts = posts
	},
	posts_latest_messages: (state, posts_latest_messages) => {
		state.posts_latest_messages = posts_latest_messages
	},



}

const actions = {
	index({commit,state}, { ...params}){
		return this._vm.$request.get('api.post.index',{...params}).then(res=>{
			if(res.data.code==0){
				// commit('posts',res.data.data.lists)
			}
			return res;
		})
	},
	publish({commit,state}, { ...params}){
		return this._vm.$request.post('api.post.publish',{...params}).then(res=>{
			if(res.data.code==0){
			}
			return res;
		})
	},
	joinGroup({commit,state}, { ...params}){
		return this._vm.$request.post('api.post.joinGroup',{...params}).then(res=>{
			if(res.data.code==0){
			}
			return res;
		})
	},

	posts({commit,state}, posts){
		console.log('posts','update')
		commit('posts',posts)
	},
	posts_latest_messages({commit,state}, posts_latest_messages){
		console.log('posts_latest_messages','update')
		commit('posts_latest_messages',posts_latest_messages)
	},
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
