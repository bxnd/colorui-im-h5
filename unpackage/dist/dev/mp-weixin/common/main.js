(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/main"],[
/* 0 */
/*!**************************************************!*\
  !*** /Users/jcc/Downloads/front-colorui/main.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(createApp) {__webpack_require__(/*! uni-pages */ 4);var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));
var _App = _interopRequireDefault(__webpack_require__(/*! ./App */ 5));
var _store = _interopRequireDefault(__webpack_require__(/*! ./store */ 11));






























































var tool = _interopRequireWildcard(__webpack_require__(/*! ./utils/tool.js */ 21));


var _request = _interopRequireDefault(__webpack_require__(/*! ./common/request.js */ 22));function _getRequireWildcardCache() {if (typeof WeakMap !== "function") return null;var cache = new WeakMap();_getRequireWildcardCache = function _getRequireWildcardCache() {return cache;};return cache;}function _interopRequireWildcard(obj) {if (obj && obj.__esModule) {return obj;}if (obj === null || typeof obj !== "object" && typeof obj !== "function") {return { default: obj };}var cache = _getRequireWildcardCache();if (cache && cache.has(obj)) {return cache.get(obj);}var newObj = {};var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;for (var key in obj) {if (Object.prototype.hasOwnProperty.call(obj, key)) {var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;if (desc && (desc.get || desc.set)) {Object.defineProperty(newObj, key, desc);} else {newObj[key] = obj[key];}}}newObj.default = obj;if (cache) {cache.set(obj, newObj);}return newObj;}function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}var friendPage = function friendPage() {Promise.all(/*! require.ensure | colorui/components/friend/page */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/friend/page")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/friend/page.vue */ 100));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var myPage = function myPage() {Promise.all(/*! require.ensure | colorui/components/my/page */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/my/page")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/my/page.vue */ 105));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var friendCirclePage = function friendCirclePage() {Promise.all(/*! require.ensure | colorui/components/friend-circle/page */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/friend-circle/page")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/friend-circle/page.vue */ 110));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var friendCirclePublish = function friendCirclePublish() {Promise.all(/*! require.ensure | colorui/components/friend-circle/publish */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/friend-circle/publish")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/friend-circle/publish.vue */ 117));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var messages = function messages() {Promise.all(/*! require.ensure | colorui/components/home/messages */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/home/messages")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/home/messages.vue */ 124));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var friends = function friends() {Promise.all(/*! require.ensure | colorui/components/friend/friends */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/friend/friends")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/friend/friends.vue */ 129));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var groups = function groups() {Promise.all(/*! require.ensure | colorui/components/friend/groups */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/friend/groups")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/friend/groups.vue */ 136));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var randomChatIndex = function randomChatIndex() {Promise.all(/*! require.ensure | colorui/components/random-chat/index */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/random-chat/index")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/random-chat/index.vue */ 141));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var cuCustom = function cuCustom() {__webpack_require__.e(/*! require.ensure | colorui/components/cu-custom */ "colorui/components/cu-custom").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/cu-custom.vue */ 146));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var cuCustomWidth = function cuCustomWidth() {__webpack_require__.e(/*! require.ensure | colorui/components/cu-custom-width */ "colorui/components/cu-custom-width").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/cu-custom-width.vue */ 151));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatAvatar = function chatAvatar() {Promise.all(/*! require.ensure | colorui/components/chat/avatar */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/avatar")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/avatar.vue */ 156));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatText = function chatText() {Promise.all(/*! require.ensure | colorui/components/chat/text */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/text")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/text.vue */ 162));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatImage = function chatImage() {Promise.all(/*! require.ensure | colorui/components/chat/image */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/image")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/image.vue */ 167));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatFile = function chatFile() {Promise.all(/*! require.ensure | colorui/components/chat/file */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/file")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/file.vue */ 172));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatVideo = function chatVideo() {Promise.all(/*! require.ensure | colorui/components/chat/video */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/video")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/video.vue */ 177));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatAudio = function chatAudio() {Promise.all(/*! require.ensure | colorui/components/chat/audio */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/audio")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/audio.vue */ 182));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatTwoClickAvatar = function chatTwoClickAvatar() {__webpack_require__.e(/*! require.ensure | colorui/components/chat/two-click-avatar */ "colorui/components/chat/two-click-avatar").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/two-click-avatar.vue */ 187));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatTwoClickText = function chatTwoClickText() {__webpack_require__.e(/*! require.ensure | colorui/components/chat/two-click-text */ "colorui/components/chat/two-click-text").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/two-click-text.vue */ 192));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatRandomChatApply = function chatRandomChatApply() {Promise.all(/*! require.ensure | colorui/components/chat/random-chat-apply */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/random-chat-apply")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/random-chat-apply.vue */ 197));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatVote = function chatVote() {Promise.all(/*! require.ensure | colorui/components/chat/vote */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/vote")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/vote.vue */ 202));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatPost = function chatPost() {Promise.all(/*! require.ensure | colorui/components/chat/post */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/post")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/post.vue */ 208));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatList = function chatList() {Promise.all(/*! require.ensure | colorui/components/chat/list */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/list")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/list.vue */ 213));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatFrom = function chatFrom() {__webpack_require__.e(/*! require.ensure | colorui/components/chat/form */ "colorui/components/chat/form").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/form.vue */ 220));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var deanpopover = function deanpopover() {__webpack_require__.e(/*! require.ensure | components/dean-popover/dean-popover */ "components/dean-popover/dean-popover").then((function () {return resolve(__webpack_require__(/*! @/components/dean-popover/dean-popover.vue */ 225));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatQuoteMessage = function chatQuoteMessage() {__webpack_require__.e(/*! require.ensure | colorui/components/chat/quote-message */ "colorui/components/chat/quote-message").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/quote-message.vue */ 232));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatQuoteMessageInputDown = function chatQuoteMessageInputDown() {__webpack_require__.e(/*! require.ensure | colorui/components/chat/quote-message-input-down */ "colorui/components/chat/quote-message-input-down").then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/quote-message-input-down.vue */ 237));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatLeft = function chatLeft() {Promise.all(/*! require.ensure | colorui/components/chat/left */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/left")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/left.vue */ 242));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var chatEmoji = function chatEmoji() {Promise.all(/*! require.ensure | colorui/components/chat/emoji */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/emoji")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/emoji.vue */ 247));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};var addUserToGroup = function addUserToGroup() {Promise.all(/*! require.ensure | colorui/components/chat/add-user-to-group */[__webpack_require__.e("common/vendor"), __webpack_require__.e("colorui/components/chat/add-user-to-group")]).then((function () {return resolve(__webpack_require__(/*! ./colorui/components/chat/add-user-to-group.vue */ 253));}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);};_vue.default.component('friend-page', friendPage);_vue.default.component('my-page', myPage);_vue.default.component('friend-circle-page', friendCirclePage);_vue.default.component('friend-circle-publish', friendCirclePublish);_vue.default.component('messages', messages);_vue.default.component('friends', friends);_vue.default.component('groups', groups);_vue.default.component('random-chat-index', randomChatIndex);_vue.default.component('cu-custom', cuCustom);_vue.default.component('cu-custom-width', cuCustomWidth);_vue.default.component('chat-avatar', chatAvatar);_vue.default.component('chat-text', chatText);_vue.default.component('chat-image', chatImage);_vue.default.component('chat-file', chatFile);_vue.default.component('chat-audio', chatAudio);_vue.default.component('chat-video', chatVideo);_vue.default.component('chat-two-click-avatar', chatTwoClickAvatar);_vue.default.component('chat-two-click-text', chatTwoClickText);_vue.default.component('chat-vote', chatVote);_vue.default.component('chat-post', chatPost);_vue.default.component('chat-random-chat-apply', chatRandomChatApply);_vue.default.component('chat-list', chatList);_vue.default.component('chat-form', chatFrom);_vue.default.component('chat-deanpopover', deanpopover);_vue.default.component('chat-quote-message', chatQuoteMessage);_vue.default.component('chat-quote-message-input-down', chatQuoteMessageInputDown);_vue.default.component('chat-left', chatLeft);_vue.default.component('chat-emoji', chatEmoji);_vue.default.component('chat-add-user-to-group', addUserToGroup);_vue.default.prototype.$tool = tool;

_vue.default.prototype.$store = _store.default;
_vue.default.prototype.$request = _request.default;
_vue.default.prototype.$api = __webpack_require__(/*! ./common/router-generator.js */ 36)(__webpack_require__(/*! ./common/route.map.js */ 37));



_vue.default.config.productionTip = false;

_App.default.mpType = 'app';

var app = new _vue.default(_objectSpread({
  store: _store.default },
_App.default));

createApp(app).$mount();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["createApp"]))

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/*!**************************************************!*\
  !*** /Users/jcc/Downloads/front-colorui/App.vue ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ 6);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=style&index=0&lang=css& */ 8);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js */ 10);
var render, staticRenderFns, recyclableRender, components
var renderjs





/* normalize component */

var component = Object(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"],
  render,
  staticRenderFns,
  false,
  null,
  null,
  null,
  false,
  components,
  renderjs
)

component.options.__file = "App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),
/* 6 */
/*!***************************************************************************!*\
  !*** /Users/jcc/Downloads/front-colorui/App.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/babel-loader/lib!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--12-1!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./App.vue?vue&type=script&lang=js& */ 7);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_12_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),
/* 7 */
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--12-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!/Users/jcc/Downloads/front-colorui/App.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}var _default =
{
  onLaunch: function onLaunch() {
    uni.getSystemInfo({
      success: function success(e) {
        _vue.default.prototype.PhoneHeight = e.windowHeight;










        _vue.default.prototype.StatusBar = e.statusBarHeight;
        var custom = wx.getMenuButtonBoundingClientRect();
        _vue.default.prototype.Custom = custom;
        _vue.default.prototype.CustomBar = custom.bottom + custom.top - e.statusBarHeight;






      } });


    if (uni.getStorageSync('token')) {
      _vue.default.prototype.$store.dispatch('getInfo');
    }
  },
  onShow: function onShow() {
    console.log('App Show');
  },
  onHide: function onHide() {
    console.log('App Hide');
  } };exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 8 */
/*!***********************************************************************************!*\
  !*** /Users/jcc/Downloads/front-colorui/App.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/postcss-loader/src??ref--6-oneOf-1-3!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!../../../../Applications/HBuilderX.app/Contents/HBuilderX/plugins/uniapp-cli/node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./App.vue?vue&type=style&index=0&lang=css& */ 9);
/* harmony import */ var _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_Applications_HBuilderX_app_Contents_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_App_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),
/* 9 */
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!./node_modules/postcss-loader/src??ref--6-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!/Users/jcc/Downloads/front-colorui/App.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
    if(false) { var cssReload; }
  

/***/ })
],[[0,"common/runtime","common/vendor"]]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/main.js.map